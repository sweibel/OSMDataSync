import os
import configparser
import re
from pathlib import Path
from qgis.core import QgsApplication

SECTION = "Settings"

OPTIONAL_CONFIGURATION = {
    "last_sync",
    "last_sync_duration",
    "description",
}

CONFIGURATIONS = {
    "overpass_query",
    "osm_id_attribute",
    "sync_state_attribute",
    "primary_key",
    "overpass_api",
}

CONFIG_ORDER = {
    "overpass_query": 1,
    "overpass_api": 2,
    "primary_key": 3,
    "osm_id_attribute": 4,
    "sync_state_attribute": 5,
    "last_sync": 6,
    "last_sync_duration": 7,
    "description": 8,
}

PLUGIN_NAME = "python/plugins/OSMDataSync"
TEMPLATE_PATH = Path(f"{QgsApplication.qgisSettingsDirPath()}{PLUGIN_NAME}/template/config_template.osmds")


def load_config_content(path):
    parser = configparser.ConfigParser(
        interpolation=configparser.ExtendedInterpolation()
    )
    try:
        with open(path, "r") as file:
            parser.read_string(file.read())
    except configparser.ParsingError:
        return False
    return parser


def load_valid_config_file(path):
    parser = load_config_content(path)
    content_dict = {}
    if not parser:
        return False, "An error occurred while parsing the config file."
    for config in CONFIGURATIONS:
        if config not in parser[SECTION]:
            return False, f"No {config} found in config file"
        content_dict[config] = parser[SECTION][config]

    for config in OPTIONAL_CONFIGURATION:
        if config in parser[SECTION]:
            content_dict[config] = parser[SECTION][config]
        else:
            content_dict[config] = None
    return True, content_dict


def update_config_file(path, last_sync, duration):
    content_dict = load_valid_config_file(path)[1]
    content_dict["last_sync"] = last_sync
    content_dict["last_sync_duration"] = duration
    write_config_file(path, content_dict)


def write_config_file(path, content_dict):
    comment_dict = save_comments(path)
    config = configparser.RawConfigParser()
    config.add_section(SECTION)
    for key, value in sorted(content_dict.items(), key=lambda kv: CONFIG_ORDER.get(kv[0])):
        if value:
            config.set(SECTION, key, value)
    with open(path, "w") as file:
        config.write(file)
    restore_comments(path, comment_dict)


def create_config_file(path):
    layer_path = Path(path)
    with open(TEMPLATE_PATH, "r") as file:
        lines = file.readlines()

    try:
        with open(layer_path.with_suffix(".osmds"), "w") as file:
            for line in lines:
                file.write(line)
    except PermissionError:
        return False
    return True


def save_comments(path):
    comment_dict = {}
    with open(path, "r") as file:
        i = 0
        lines = file.readlines()
        for line in lines:
            if re.match(r"^\s*(#|;).*?$", line):
                comment_dict[i] = line
            i += 1
    return comment_dict


def restore_comments(path, comment_dict):
    with open(path, "r") as file:
        lines = file.readlines()
    for i, comment in sorted(comment_dict.items()):
        lines.insert(i, comment)
    with open(path, "w") as file:
        file.write("".join(lines))


def found_config_file(layer):
    if layer:
        return os.path.isfile(get_config_file_path(layer))
    return False


def get_config_file_path(layer):
    filepath = layer.dataProvider().dataSourceUri()
    return get_config_path(filepath)


def get_config_path(filepath):
    return os.path.splitext(filepath)[0] + ".osmds"
