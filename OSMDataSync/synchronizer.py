import json
import osm2geojson
from qgis.core import QgsFeature, QgsFileDownloader, QgsGeometry
from qgis.PyQt.QtCore import QDir, QUrl, QUrlQuery, QEventLoop
from shapely.geometry import shape
from .helper import edit_attribute, field_names, get_field_id


class Synchronizer:
    def __init__(
        self, progress_bar, layer, state_field_name, osm_id_field_name, primary_key_name, overpass_api
    ):
        self.STATE_FIELD_NAME = state_field_name
        self.OSM_ID_Field_NAME = osm_id_field_name
        self.OVERPASS_API = overpass_api
        self.LAYER = layer
        self.PROGRESS_BAR = progress_bar
        self.META_FIELDS = [
            primary_key_name,
            state_field_name,
            osm_id_field_name,
        ]

        self.error_message = None

        self.result_path = f"{QDir.tempPath()}/request-XXXXXX.json"

        self.LAYER.editingStopped.connect(self.force_editing)

    def synchronize(self, config_dict, osm_id_feature_dict, del_not_found):
        found_feature_keys = set()
        json = self.query(config_dict["overpass_query"])

        if json is None:
            return False, self.error_message
        if not json:
            return False, "The query did not return JSON"
        geojson = osm2geojson.json2geojson(json)

        for nr, osm_object in enumerate(geojson["features"]):
            self.update_progress(nr, len(geojson["features"]))
            feature_key = self.compare_osm_objects(osm_object, osm_id_feature_dict)
            found_feature_keys.add(feature_key)

        self.mark_not_found_features(found_feature_keys, osm_id_feature_dict, del_not_found)
        return True, ""

    def mark_not_found_features(self, found_feature_keys, osm_id_feature_dict, del_not_found):
        not_found_feature_keys = osm_id_feature_dict.keys() - found_feature_keys
        for not_found_feature_key in not_found_feature_keys:
            if (
                    osm_id_feature_dict[not_found_feature_key][self.STATE_FIELD_NAME] == "not found"
                    and del_not_found
            ):
                self.delete_feature_by_id(osm_id_feature_dict[not_found_feature_key])
            edit_attribute(self.LAYER, self.STATE_FIELD_NAME, osm_id_feature_dict[not_found_feature_key], "not found")

    def compare_osm_objects(self, osm_object, osm_id_feature_dict):
        key = self.key_from_osm_object(osm_object)
        if key in osm_id_feature_dict.keys():
            if not self.ignore_feature(osm_id_feature_dict[key]):
                feature = osm_id_feature_dict[key]
                old_state = self.feature_field_attributes_dict(feature)[self.STATE_FIELD_NAME] 
                new_state = self.compare_osm_object(osm_object, feature)
                if new_state == "changed":
                    self.update_feature(osm_object, feature, "changed")
                if new_state == "unchanged" and (old_state != "unchanged" or old_state == "NULL"):
                    edit_attribute(self.LAYER, self.STATE_FIELD_NAME, feature, "unchanged")
        else:
            self.new_feature(osm_object)
        return key

    def compare_osm_object(self, osm_object, feature):
        if (self.same_geometry(osm_object, feature) 
           and self.same_field_values(osm_object, feature)
        ):
            return "unchanged"
        return "changed"

    def same_geometry(self, osm_object, feature):
        return feature.geometry().isGeosEqual(self.geometry_from_osm_object(osm_object))

    def same_field_values(self, osm_object, feature):
        same = True
        feature_field_attributes_dict = self.feature_field_attributes_dict(feature)
        for field in field_names(feature):
            if (
                "tags" in osm_object["properties"].keys()
                and field in osm_object["properties"]["tags"].keys()
                and (not feature_field_attributes_dict[field] == osm_object["properties"]["tags"][field])
            ):
                same = False
        return same

    def update_feature(self, osm_object, old_feature, state):
        feature_field_attributes_dict = self.feature_field_attributes_dict(old_feature)
        self.delete_feature_by_id(old_feature)
        feature = QgsFeature(self.LAYER.fields())
        for field in field_names(feature):  
            if field[0] == "@" and field not in self.META_FIELDS:
                feature.setAttribute(field, feature_field_attributes_dict[field])
            elif (
                "tags" in osm_object["properties"].keys()
                and field in osm_object["properties"]["tags"].keys()
            ):
                feature.setAttribute(field, osm_object["properties"]["tags"][field])
        feature.setId(old_feature.id())
        feature.setAttribute(self.STATE_FIELD_NAME, state)  # "changed" or "unchanged" (or "new")
        key = self.key_from_osm_object(osm_object)
        feature.setAttribute(self.OSM_ID_Field_NAME, key)
        feature.setGeometry(self.geometry_from_osm_object(osm_object))
        self.LAYER.addFeature(feature)

    def new_feature(self, osm_object):
        feature = QgsFeature(self.LAYER.fields())
        if "tags" in osm_object["properties"].keys():
            for field in self.LAYER.fields():
                for osm_field in osm_object["properties"]["tags"].keys():
                    if osm_field in field.name():
                        feature.setAttribute(osm_field, osm_object["properties"]["tags"][osm_field])

        feature.setAttribute(self.STATE_FIELD_NAME, "new")
        key = self.key_from_osm_object(osm_object)
        feature.setAttribute(self.OSM_ID_Field_NAME, key)
        feature.setGeometry(self.geometry_from_osm_object(osm_object))
        self.LAYER.addFeature(feature)

    def delete_feature_by_id(self, feature):
        self.LAYER.deleteFeature(feature.id())

    def ignore_feature(self, feature):
        state_attribute_id = get_field_id(self.LAYER, self.STATE_FIELD_NAME)
        fields_to_ignore = ["local", "ignore"]

        if feature[state_attribute_id] in fields_to_ignore:
            return True
        return False

    @staticmethod
    def feature_field_attributes_dict(feature):
        fields = [field.name() for field in feature.fields()]
        attributes = feature.attributes()
        return dict(zip(fields, attributes))

    def query(self, query):
        url_query = QUrl(self.OVERPASS_API)

        query_string = QUrlQuery()
        query_string.addQueryItem("data", query)
        query_string.addQueryItem("format", "json")
        url_query.setQuery(query_string)

        loop = QEventLoop()
        downloader = QgsFileDownloader(
            url_query, self.result_path, delayStart=True
        )

        downloader.downloadExited.connect(loop.quit)
        downloader.downloadError.connect(self.network_error)
        downloader.startDownload()
        loop.exec_()

        if self.error_message:
            return None
        with open(self.result_path) as json_file:
            try:
                data = json.load(json_file)
            except:
                return False
            return data

    def network_error(self, messages):
        error_message = ""
        if "Download failed: Host" in messages[0]:
            error_message = "Couldn't reach the Host"
        elif "Download failed: Error transferring" in messages[0]:
            error_message = "Couldn't download the file: Error in the Query"
        self.error_message = error_message

    @staticmethod
    def key_from_osm_object(osm_object):
        return f"{osm_object['properties']['type']}/{osm_object['properties']['id']}"

    @staticmethod
    def geometry_from_osm_object(osm_object):
        return QgsGeometry.fromWkt(shape(osm_object["geometry"]).wkt)

    def force_editing(self):
        self.LAYER.startEditing()

    def update_progress(self, nr, amount):
        progress = int(nr / amount * 100)
        self.PROGRESS_BAR.setProgress(progress)
