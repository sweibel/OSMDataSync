import datetime
import time
from qgis.core import Qgis, QgsProject


def get_field_id(layer, field_name):
    state_attribute_id = -1
    for nr, field in enumerate(layer.fields()):
        if field_name == field.name():
            state_attribute_id = nr
            break
    return state_attribute_id


def select_feature(layer, feature):
    if Qgis.QGIS_VERSION_INT >= 21600:
        layer.selectByIds([feature.id()])
    else:
        layer.setSelectedFeatures([feature.id()])


def amount_of_features(layer):
    return len(list(layer.getFeatures()))


def field_names(feature):
    return [field.name() for field in list(feature.fields())]


def edit_attribute(layer, field, feature, selected_state):
    if layer.isEditable():
        state_attribute_id = get_field_id(layer, field)
        attrs = {state_attribute_id: selected_state}
        layer.changeAttributeValues(feature.id(), attrs)
        feature[state_attribute_id] = selected_state
        return True
    return False


def get_layer_from_id(lay_id):
    return QgsProject.instance().mapLayer(lay_id)


def passed_sec(start_time):
    seconds = time.time() - start_time
    return f"{int(seconds)}"


def date_time():
    return datetime.datetime.today().strftime("%x %X")
