# OSM Data Sync QGIS Plugin

** *NOTE:* This plugin is expertimental! Be cautious about user defined local attributes (all beginning with "@" like "@comment") since they may be lost after synchronization. Therefore make a backup of the dataset before synchronizing.**

This is an experimental plugin to maintain an own local geospatial dataset, keeping it in sync with OpenStreetMap (OSM). 

See this white paper on ["Synchronizing Local Datasets and OpenStreetMap using QGIS"](https://md.coredump.ch/s/H1IQbLzjU).

The Python code is based on experiences by extending the ["Go2NextFeature3+ QGIS plugin"](https://gitlab.com/geometalab/transfer-praktikanten-2019/Go2NextFeature3/).


## How to use

Once the plugin is installed and activated, this GUI shows up:

![dialog](images/dialog.png)
1. In this combobox every layer that is currently loaded in the project are found. Choose the layer you want to work with.
1. When this button is clicked, it attempts to create a config file when the configuration state is set to "Not found". You'll have to edit the file then, as it is just a template. If the config state is set to "Invalid" or "OK", this button just reloads the file.
1. Synchronizing only works when the chosen layer is in edit mode. When clicking the button, you are asked first if you would like to proceed and if features with their synchronization state set to "Not found" should be deleted by the synchronization process. When confirmed, the query in your config file is used to get the data from OSM. How the synchronization process works is explained in the white paper.
1. When going through all features, you can choose whether the map should just pan (move to the feature) or also zoom to it.
1. `Select feature` selects the features you are currently viewing (and deselects all others), when going through them.
1. In this combobox every attribute your features have are found. When going through all the features, they will be ordered by the chosen attribute.
1. In this field the id of the feature that is currently viewed is displayed. The id should be an OSM-id e.g. "node/2238796065". `Show on OSM` opens a browser tab and searches the feature on openstreetmap with the given OSM-id.
1. When going through all the features, it is shown what its synchronization state attribute is. You can click on another state to change it. Some changes don't work, such as from "new" to "local". Other restrictions are found in the white paper.
1. With these buttons you can go through all the features.


## How to install

1. Download or open the folder `OSMDataSync` as a zip-file
1. Make sure to pick exactly the folder `OSMDataSync`, extract the contents of this folder 
1. Then copy the contents in `YOUR_PATH/QGIS/QGIS3/profiles/YOUR_PROFILE/python/plugins`
e.g. `C:\Users\username\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins`
(these 3 first steps can be done in one single step)
1. Open or restart QGIS 3.X.X
1. Go to 'Plugins' -> 'Manage and Install Plugins...'
1. Look for 'OSMDataSync' in the list of plugins and enable it with a click


## How to set up a configuration file

If you want to work with the OSM Data Sync Plugin, you need to create a configuration file for each of your datasets. Such a file has to be in the same folder as its dataset and it must have the same name but with the filename extension ".osmds".

So for example, you are working with the dataset "thermal_baths.gpkg", which contains all the thermal baths in Switzerland. Its config-file "thermal_baths.osmds" should look somewhat like this:

```
[Settings]
overpass_query = 
	[out:json];
	area["name"="Schweiz/Suisse/Svizzera/Svizra"];
	nwr["bath:type"=thermal](area);
	out center;
sync_state_attribute = @sync_state
primary_key = fid
osm_id_attribute = @id
overpass_api = https://overpass.osm.ch/api/interpreter
description = Thermal baths in Switzerland
```

All these keys are essential, except for "description". Each key needs of course a valid value:
* __overpass_query:__ Here should be the same query that was used to generate the dataset with overpass.
* __sync_state_attribute:__ For every dataset you have to create an attribute that you'll put here. This attribute contains the current synchronization state of its feature, such as "new" or "unchanged".
* __primary_key:__ A feature attribute that functions as an unique identifier.
* __osm_id_attribute:__ A feature attribute that contains the OSM-id of a feature, e.g. "node/2238796065".
* __overpass_api:__ The URL of the overpass API you get your data from.
* __description:__ A description of your dataset. It can only be found in the config file.


## Tips and Tricks for the "Order by" feature

When working with big datasets the "Order by" feature can come in quite handy. You might often use it to sort the features in your dataset by their synchronization state. But what if you want to sort with another, secondary attribute? This Plugin doesn't provide a solution for this (yet), but there is another way to accomplish this, with virtual fields. Here's how you create one:

* Go to your dataset's `Layer Properties` -> `Fields`
* Click on `Field Calculator`
* Tick the `Create a new field` and the `Create virtual field` boxes
* For this example, we'll name the field `@sort`
* Set the field type to `Text (string)`
* Click on the collapsable field `Fields and Values`, where you can find all the attributes
* Double-click on your synchronization state attribute to add it to the Expression text-box
* Add `||`, `'/'` (with the apostrophe) and another `||` to the Expression, and finally add another attribute, e.g. `name`
* Click on `OK`, which adds the attribute, that is always the combination of both attributes as a string, e.g. "local/Aquandeer"

You can also do this with more than two attributes. This virtual attribute can be used to sort with the "Order by" feature.